const nodemailer = require('nodemailer');
// const {mailHostEmail, mailHostPassword, mailHost, mailPost} = require('../utils/mailConfig');
const { mailConfiguration } = require('../utils/mailConfig');
const { notFound, handleSuccessResponse, badRequest, validationError, recordExists } = require('../utils/response-helper');

const {CandidateResumes} = require('../models/candidate_resumes');

var config = mailConfiguration();

var host = config.mailHost;
var port = config.mailPort;
var hostEmail = config.hostEmail;
var hostPassword = config.hostPassword;

/* Send single email */
exports.sendEmail = (req, res, next) => {
    
    var mailText = req.body.mail_text;
    var candidateEmail = req.body.mail_id;
    
    const transporter = nodemailer.createTransport({
        host: host,
        port: port,
        secure: true,
        auth: {
            user: hostEmail,
            pass: hostPassword
        }
    });

    var mailOptions = {
        from: hostEmail,
        to: candidateEmail,
        subject: 'Test Email Subject',
        html: '<h1>'+mailText+'</h1>'
    };
    
    // send email
    transporter.sendMail(mailOptions, function(error, info){

        try {            
            if (error) {
                return badRequest('Mail not sent');
            } else {
                return handleSuccessResponse(res, 'Mail sent successfully');
            }
        } catch (error) {
            return badRequest('Mail not sent');
        }
    });


}

/* Send multiple email */
exports.sendEmailMultiple = async (req, res, next) => {
    
    const errorCount = [];
    var mail_body = req.body.mail_text;
    var mail_ids = req.body.mail_ids;
    
    const transporter = nodemailer.createTransport({
        host: host,
        port: port,
        auth: {
            user: hostEmail,
            pass: hostPassword 
        }
    });
    
    mail_ids.map( async (id, indx) => {
        
        const candidateData = await CandidateResumes.findById(id);
        
        var mailOptions = {
            from: hostEmail,
            to: candidateData.email,
            subject: 'Test Email Subject',
            html: '<p>'+mail_body+'</p>'
        };

        // send email 
        transporter.sendMail(mailOptions, function(error, info){

            if (error) {
                errorCount.push(candidateData.email);
            }

        });
        
    });

    if (errorCount.length == 0) {
        /*const data = {
            data : 'Mails sent successfully'
        }*/
        return handleSuccessResponse(res, 'Mails sent successfully');        
    } else {
        return badRequest('Mail not sent');        
    }

}