const {validationResult} = require('express-validator');
const opn = require('opn');
const multer = require('multer')
const path = require('path')
const mime = require('mime')
const fs = require('fs')
const _ = require('lodash');

const bcrypt = require('bcryptjs');

const User = require('../models/user');
const UserRole = require('../models/UserRole');
const { CandidateStatus } = require("../models/candidate_status");
const {CandidateResumes} = require('../models/candidate_resumes');
const { SkillsMaster } = require("../models/skills_master");
const CandidateResumeStatus = require("../models/candidate_resume_status");
const {CandidateDuplicates} = require('../models/candidate_duplicates_resumes');


const parseIt = require('../src/parseIt');
const { notFound, handleSuccessResponse, badRequest, validationError, recordExists } = require('../utils/response-helper');
const { pagination, isObjectIdValid,getCurrentUserRoleId, sendMail,getCurrentUserAssignedUser } = require('../utils/helper');
let mongoose = require('mongoose');
const {Country, State, City} = require('../models/country');

/**method to fetch all record */
RegExp.escape = function(s) {
    return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};

exports.getResumes = async (req, res, next) => {
    try {
         
        let userId = req.userId ? req.userId :''
        let userroleid = req.userRole ? req.userRole :''
        let {page, skills, name, email, phone, city, company, sortingData, status, minExp, maxExp} = req.query;
        let sortBy
        let orderBy 
        const currentPage = parseInt(page) || 1;
        const perPage = parseInt(process.env.RECORDS_PER_PAGE);
        
        //get current logged user role id
        // const userids=await getCurrentUserRoleId(userroleid,userId);
        const userids=await getCurrentUserAssignedUser(userId);
        var UserObjectId = mongoose.Types.ObjectId(userId); 
        userids.push(UserObjectId);
        
        //by skills
        let searchbyskill = null;
        if (!_.isEmpty(skills)) {
            const skill = req.query.skills;
            const skillsregex = [];
            skill.map((el) => {
                let re = new RegExp(el.trim(), 'i');
                skillsregex.push(re);
            });

            /** line commented by lokesh **/
            searchbyskill = { skills: { $in: skillsregex } }
            // searchbyskill = { skills: { $in: skill } }

        }
        //by name

        let searchbyname;
        if (!_.isEmpty(name)) {
            const re = new RegExp(req.query.name.trim(), 'i');
            searchbyname = { name: re }
        }

        /** code added by lokesh **/
        //by email
        let searchbyemail;
        if (!_.isEmpty(email)) {
            const re = new RegExp(req.query.email.trim(), 'i');
            searchbyemail = { email: re }
        }

        let searchbyphone;
        if (!_.isEmpty(phone)) {
        
            const phoneno = new RegExp(RegExp.escape(req.query.phone.trim()), 'i');
            searchbyphone = { phone: phoneno }
        }

        let searchbycity;
        if (!_.isEmpty(city)) {
            const city = new RegExp(req.query.city.trim(), 'i');
            searchbycity = { place: city }
        }

        let searchbycompany;
        if (!_.isEmpty(company)) {
            const company = new RegExp(req.query.company.trim(), 'i');
            searchbycompany = { workExperience : company }
        }

        let userlists;
        if(!_.isEmpty(userids)){
            userlists = { created_by: { $in: userids } }
        }

        let searchbystatus;
        if(!_.isEmpty(status)){
            searchbystatus = { candidate_status : { $eq: status }  }
        }

        let searchbyexp;
        if(!_.isEmpty(minExp)){
            searchbyexp = {  total_experience : { $gte: parseFloat(minExp), $lte: parseFloat(maxExp) } }
            // searchbyexp = { $or: [{  
            //     total_experience : {'$gte': minExp, '$lte': maxExp},
                
            // }]
            

        } 

        let searchByDel = {is_deleted : { $eq: '0' }}
        let search;
        if(req.userRole === '1'){
            search = Object.assign(
                {}, 
                searchbyskill, 
                searchbyname, 
                searchbyemail, 
                searchbyphone, 
                searchbycity, 
                searchbycompany , 
                searchbystatus,
                searchByDel,
                searchbystatus,
                searchbyexp
            );
        }else{
            search = Object.assign(
                {}, 
                searchbyskill, 
                searchbyname, 
                searchbyemail, 
                searchbyphone, 
                searchbycity, 
                searchbycompany , 
                searchbystatus,
                searchByDel,
                userlists,
                searchbystatus,
                searchbyexp
            );
        }
        
        //run search

        const totalItems = await CandidateResumes.aggregate([ {$match : search }]);
        const sorting = JSON.parse(sortingData)
        let sortObject = {}
        if(_.isEmpty(sorting)){
            sortBy = '_id'
            orderBy = -1
        }else{
            sortBy = sorting.name
            orderBy = sorting.order === 'asc' ? 1 : -1
            
        }
        sortObject[sortBy] = orderBy
        const user_resume = await CandidateResumes.aggregate([ 
                                    {$match : search },
                                    { $sort : sortObject },
                                    { $skip:  (currentPage - 1) * perPage },
                                    { $limit : perPage }
                                    ])
                                    // .sort(sortObject)
                                    // .skip((currentPage - 1) * perPage).limit(perPage);

        
        const skillList = await SkillsMaster.find({is_deleted : { $eq: '0' }});
        const candidateStatusList = await CandidateStatus.find({});
        
        let statusList=[];
        if(!_.isEmpty(candidateStatusList)){
            statusList= candidateStatusList.map(list=>{
                    return{
                         "label" : list.name,
                         "value" : list.pid
                    }
            })
        }
        let finalData = [];
        //console.log(user_resume);
        if(user_resume){
            finalData = user_resume.map(obj => {
                var date = new Date(obj.created_at)
                return {
                    "id"               : obj._id,
                    "name"             : obj.name,
                    "email"            : obj.email,
                    "phone"            : obj.phone,
                    "resumePath"       : obj.resumePath,
                    "candidate_status" : obj.candidate_status,
                    "created_at"       : date.getDate() +  " " + date.toLocaleString('default', { month: 'long' }) + " " + date.getFullYear(),
                }
            });

            const paginationData = pagination(currentPage, totalItems.length , 'resumes');

            const data = {
                resumes     : finalData,
                skills      : skillList,
                applicant_status:statusList,
                candidateStatusList : statusList,
                ...paginationData
            }
         
            return handleSuccessResponse(res, data)
        }
        
        throw notFound('No Records Found')

    } catch (err) {
        // console.log('err',err);
        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}

/* method to get candidate details by id */
exports.getCandidate = async (req, res, next) => {
    try {
        const id = req.params.candidateId;
        
        if(!isObjectIdValid(id))
            throw notFound('Record Not Found.');

        let finalData = [];
        const candidateData = await CandidateResumes.findById(id);
        if(candidateData){
            let stateObj = {}
            let cityObj = {}
            let countryName,stateName="";
            
            if(candidateData.country){
                countryName   = await Country.findById(candidateData.country);
               
                stateObj.country = candidateData.country
            }
            if(candidateData.state){
                cityObj.state = candidateData.state;
                stateName   = await State.findById(candidateData.state);
            }
            
            const country   = await Country.find({});
            const state     = await State.find(stateObj);
            
            
            finalData = {
                "id"                : candidateData._id,
                "name"              : candidateData.name,
                "email"             : candidateData.email,
                "phone"             : candidateData.phone,
                "place"              : candidateData.place,
                "skills"            : candidateData.skills,
                "resumePath"        : candidateData.resumePath,
                "workExperience"    : candidateData.workExperience,
                "candidate_status"  : candidateData.candidate_status,
                "dob"               : candidateData.dob ,
                "location"          : candidateData.address ,
                "exp"               : candidateData.total_experience ,
                "designation"       : candidateData.designation ,
                "current_ctc"       : candidateData.current_ctc ,
                "expected_ctc"      : candidateData.expected_ctc ,
                "resume_label"      : candidateData.resume_label,
                "country"           : candidateData.country,
                "state"             : candidateData.state,
                "zip"               : candidateData.zip,
                "country_name"      : (countryName!='')?"":countryName.name,
                "state_name"        : (stateName!='')?"":stateName.name
            }

            const data = { candidate : finalData, country:country,state:state }

            return handleSuccessResponse(res, data);
        }
        
        throw notFound('No Record Found');

    } catch(err){
        if (!err.status)
            err.status = 500;

        next(err);
    }
}

/* Update candidate details */
/**method to update an existing record */
exports.updateCandidate = async (req, res, next) => {
    try{
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            throw validationError(errors.errors)
        }
        
        const candidateId = req.params.candidateId;
        if(!isObjectIdValid(candidateId))
            throw notFound('Record Not Found.');

        const {
            name, 
            email, 
            phone, 
            skills, 
            workExperience, 
            dob, 
            location, 
            exp, 
            designation, 
            current_ctc, 
            expected_ctc, 
            resume_label, 
            country, 
            state, 
            place, 
            zip,
            loginFor 
        } = req.body;

        // console.log("Body: ", req.body);return;

        /*const resumeExists = await CandidateResumes.findOne({ email: email });
        if (resumeExists)
            throw recordExists('A resume with this email already exists.');

        const resumePhoneExists = await CandidateResumes.findOne({ phone: phone });
        if (resumePhoneExists)
            throw recordExists('A resume with this phone number already exists.');*/

        let fileName = null;
        if(req.file)
            fileName = req.file.filename;
        
        const candidate = await CandidateResumes.findById(candidateId);
        if(!candidate)
            throw notFound('No Records Found');
        
        candidate.name              = name;
        candidate.email             = email;
        candidate.phone             = phone;
        candidate.skills            = skills;
        candidate.place             = place;
        candidate.workExperience    = workExperience;
        candidate.resumePath        = fileName;
        candidate.dob               = dob;
        candidate.address           = location;
        candidate.total_experience  = exp;
        candidate.designation       = designation; 
        candidate.current_ctc       = current_ctc; 
        candidate.expected_ctc      = expected_ctc; 
        candidate.resume_label      = resume_label;
        candidate.country           = country;
        candidate.state             = state;
        candidate.zip               = zip;
        candidate.is_deleted        = "0";

        const result    = await candidate.save();
        const isCandidateLogin = loginFor === 'candidate' ? true : false
        if(result) {
            const data = { message : 'Candidate Details Updated Successfully', isCandidateLogin, id: candidateId }
            return handleSuccessResponse(res, data);
        }
            
        throw badRequest('Some Error Occured');

    } catch(err){
        if (!err.status)
            err.status = 500;
        
        next(err);
    }
}

/**method to fetch all record */
exports.getFile = async (req, res, next) => {
    try {
        const name = req.params.filename;
        opn("./uploads/" + name);
        res.download("./uploads/" + name);

    } catch (err) {
        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}

/* Method to fetch all record */
exports.parseFile = async (req, res, next) => {
    try {
        if (req.body.filename) {
            var names = req.body.filename;
            var str_array = names.split(',');
            var promiseArray = [];

            for (var i = 0; i < str_array.length; i++) {
                let path_upload = 'uploads/' + str_array[i];
                // parseIt.parseResume(path_upload, './compiled',req.userId);
                promiseArray.push(getexitsemail(path_upload));	
            }

            //////////
            var promiseData = Promise.all(promiseArray).then(function(values) {
                return new Promise((resolve,reject)=>{
                    return resolve(values);
                });
            }).catch(error => {
                return handleSuccessResponse(res, { data : 'Resume not added' });
            });
            
            ////////////
            promiseData.then(function(result){
                //console.log(result);
                var newarray =[];
                for(let i =0;i<result.length;i++){

                    if(result[i].output == true){
                        newarray.push(result[i].file);
                    } else{
                        parseIt.parseResume(result[i].file, './compiled',req.userId, req.body.frontendURL);
                    }
                    // else{
                    //     parseIt.parseResume(result[i].file, './compiled',req.userId);
                    // }
                    
                }
                
                var duplicate_data =  JSON.stringify(newarray);
                const candidatesduplicates = new CandidateDuplicates();
                candidatesduplicates.duplicates = duplicate_data;
                candidatesduplicates.save();
            });
            
            ////////////////////////
            var dupicate_records = (await CandidateDuplicates.find({}).sort({_id: -1}).limit(1))[0];
            let duplicate_candidate = dupicate_records.duplicates;
            
            if(duplicate_candidate.length != 0){

                //return handleSuccessResponse(res, { data : duplicate_candidate});
                //return handleSuccessResponse(res, { data : 'Resume of '+ duplicate_candidate + ' are already exits, and others resumes are uploaded successfully'});
                return handleSuccessResponse(
                    res, 
                    { 
                        data : 'Resumes are uploaded successfully', 
                        preExistEmails: duplicate_candidate
                    }
                );
                // return handleSuccessResponse(res, { data : 'Resume of '+ duplicate_candidate + ' are already exits, and oothers resumes are uploaded successfully'});

            } else{
                return handleSuccessResponse(res, { data : 'Resume Added Successfully', preExistEmails: false });
            }
        }
        //res.status(200).json({ message: 'Resume Added Successfully' });
        // return handleSuccessResponse(res, { data : 'Resume Added Successfully' });

    } catch (err) {
        console.log('Error');
        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}

/* method for existing resumes */
function getexitsemail(path_upload) {
    return new Promise((resolve, reject) => {
        parseIt.resumeCheck(path_upload, function(file, error) {

            if (error == true) {
                return reject(error);
            }
            if(file.output == false){
                return resolve(file);
            } 
            if(file){
                return resolve(file);
            } 
        
        });
    
    });
}

/**method for saving a new record */
exports.saveManualResume = async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty())
            throw validationError(errors.errors)

        const {name, email, phone, skills, workExperience, dob, location, exp, designation, current_ctc, expected_ctc, resume_label, country, state, place, zip  } = req.body;
        
        const resumeExists = await CandidateResumes.findOne({ email: email });
        if (resumeExists)
            throw recordExists('A resume with this email already exists.');

        const resumePhoneExists = await CandidateResumes.findOne({ phone: phone });
        if (resumePhoneExists)
            throw recordExists('A resume with this phone number already exists.');
        
        let fileName = null;
        if(req.file)
            fileName = req.file.filename;
        
        //currently password is not taken by manual side so passing this static password on mail as well as on table.
        const password = '123456'

        /* Mail body and Subject start */
        const url = `${req.body.frontendURL}/login/candidate`
        req.body.mailBody = `<div style = "margin-top: 20px;">
                                <p style= "font-size : 1.8rem;">Welcome! <strong>${name},</strong></p> <br /> 
                                <p>Your account is created on Virtual Employee portal, please use this link <a href=${url}>${ url } </a> to login with your email <strong>${email}</strong> and your password <strong>${password}<strong>.</p>
                            </div>`
        req.body.mailSubject =  'Login on Virtual Employee Portal'
        /* Mail body and Subject end */

        const hashedPw = await bcrypt.hash(password, 12);
        var resume = new CandidateResumes({
            name,
            email,
            phone,
            skills,
            place:place,
            workExperience,
            resumePath : fileName,
            created_by : req.userId,
            dob: dob,
            address: location, 
            total_experience :exp, 
            designation: designation, 
            current_ctc: current_ctc, 
            expected_ctc: expected_ctc, 
            resume_label:resume_label,
            candidate_status:1,
            country:country,
            state:state,
            zip:zip,
            is_deleted:"0",
            password: hashedPw  /* adding default password for candidate */

        });
        
        const result = await resume.save();

        const resumeStatus = new CandidateResumeStatus();
        resumeStatus.candidate_id       = result._id;
        resumeStatus.candidate_status   = 1;
        resumeStatus.user_id            = req.userId;
        await resumeStatus.save();
        if(result){
            await sendMail(req.body)
            return handleSuccessResponse(res, { data : 'Resume Added Successfully' });
        }
        
        throw badRequest('Some Error Occured');

    } catch(err){
        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}

// handle storage using multer
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads');
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`);
    }
})

var upload = multer({ storage: storage });

const decodeBase64Image = (dataString) => {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
      response = {};
  
    if (matches.length !== 3) {
      return new Error('Invalid input string');
    }
  
    response.type = matches[1];
    response.data = new Buffer.from(matches[2], 'base64');
  
    return response;
}


exports.updateStatusField= async(req,res,next)=>{
    try{
        
        let {id,candidate_status} = req.body; 
        if(id==''){
            return new Error('Invalid Id');
        }

        const status_based = await CandidateResumes.findById(id);
        if(!status_based)
            throw notFound('No Records Found');

        status_based.candidate_status = candidate_status;
        const result    = await status_based.save();

        const resumeStatus = new CandidateResumeStatus();
        resumeStatus.candidate_id = id;
        resumeStatus.candidate_status = candidate_status;
        resumeStatus.user_id = req.userId;
        resumeStatus.note = 'Status Changed';
        await resumeStatus.save();

        if(result) {
            const data = { data : 'Status Updated Successfully' }
            return handleSuccessResponse(res, data);
        }
            
        throw badRequest('Some Error Occured');    
    }catch(error){

    }
    
}

exports.candidateCommunication = async(req, res, next)=>{

    try{
        let id = req.params.candidateId;
         
        if(!isObjectIdValid(id)){
            return new Error('Invalid Id');
        }
        
        var ObjectId = mongoose.Types.ObjectId(id); 
        var query = { candidate_id: ObjectId };
        const resume_status = await CandidateResumeStatus.aggregate([
            {
                $match:{ candidate_id: ObjectId }
            },
            {
                $lookup:
                  {
                    from: 'candidate_resumes',
                    localField: "candidate_id", 
                    foreignField: "_id", 
                    as: "candidate",
                    
                  }
             },
             {$unwind: '$candidate'},
             {
                $lookup: {
                    from: "users", 
                    localField: "user_id", 
                    foreignField: "_id", 
                    as: "user" 
                 }
             },
             {$unwind: '$user'},
             {
                $lookup: {
                    from: "candidate_status", 
                    localField: "candidate_status", 
                    foreignField: "pid", 
                    as: "candidate_status_name" 
                 }
             },
            // {$unwind: '$candidate_status_name'},
             {
                "$project": {
                  "_id": 1,
                  "candidate_id": 1,
                  "note": 1,
                  "created_at": 1,
                  "candidate_status":1,
                  "user_id":1,
                  "candidate.email": 1,
                  "candidate.name": 1,
                  "user.first_name": 1,
                  "user.last_name": 1,
                  "user.email":1,
                  "candidate_status_name":1
                
                }
             },
             {$sort: {"created_at": -1}},
        ]);
        
        if(!resume_status)
            throw notFound('No Records Found');

        if(resume_status) {
            
            return handleSuccessResponse(res, resume_status);
        }    
    }catch(err){
        if (!err.status)
        err.status = 500;
        
    next(err);
    }

}


exports.addCandidateComment = async (req, res, next) => {
    try{

        let { candidate_id,comment } = req.body; 
        if(!isObjectIdValid(candidate_id)){
            return new Error('Invalid Id');
        }

        const current_status = await CandidateResumes.findById(candidate_id);
        if(!current_status)
            throw notFound('No Records Found');

        
        const resumeStatus = new CandidateResumeStatus();
        resumeStatus.candidate_id       = candidate_id;
        resumeStatus.candidate_status   = current_status.candidate_status;
        resumeStatus.note               = comment
        resumeStatus.user_id            = req.userId;
        await resumeStatus.save();

        if(resumeStatus) {
            const data = { data : 'Note Added Successfully' }
            return handleSuccessResponse(res, data);
        }
            
        throw badRequest('Some Error Occured'); 

    }catch(err){
        if (!err.status)
            err.status = 500;
        
        next(err);
    }

}

/**method to remove the file */
exports.removeFile = async (req, res, next) => {
    try {
        const { filename } = req.body;
        const path = './uploads/'+filename;
        // fs.unlink(path, (err) => {
        //     if (err) {
        //         console.error(err)                
        //     }
        //     return handleSuccessResponse(res, { data : 'File Removed Successfully' });
        // })
    } catch (err) {
        if (!err.status)
            err.status = 500;  
        next(err);
    }
}

exports.getCountry = async(req, res, next) =>{

    try{

        const country = await Country.find({});
        if(country) {

            const data = { country : country }
            return handleSuccessResponse(res, data);
        }

        throw badRequest('Some Error Occured'); 

    }catch(err){
        if (!err.status)
            err.status = 500;
        
        next(err);
    }
}

exports.getState = async(req, res, next) =>{

    try{

        let id = req.params.countryId;
        const state = await State.find({country:id});
        if(state) {
            
            const data = { state : state }
            return handleSuccessResponse(res, data);
        }

        throw badRequest('Some Error Occured'); 

    }catch(err){
        if (!err.status)
            err.status = 500;
        
        next(err);
    }
}

/* method to delete record */
exports.deleteResume = async (req, res, next) => {
    try {
        const id = req.params.resumeId;
        const deletingUser = req.userId;
        const admin = '61c332c84855ec3c90dc588b';
        
        if(!isObjectIdValid(id))
            throw notFound('Record Not Found.');

        const candidate = await CandidateResumes.findById(id);
        
        const creatorId = candidate.created_by;

        if (deletingUser == admin || deletingUser ==  creatorId) {
            
            candidate.is_deleted  = "1";
            const result    = await candidate.save();
            
            if(result){
                const data = {
                    data : 'Candidate Deleted Successfully'
                }
                return handleSuccessResponse(res, data);
            }
            
            throw badRequest('Some Error Occured');

        } else {
            throw badRequest('Only admin or creator of this resume can delete is data');
        }

    } catch(err){
        if (!err.status)
            err.status = 500;

        next(err);
    }
}

exports.changePassword = async (req, res, next) => {

    try {

        const {password, candidateId } = req.body;
        const candidate = await CandidateResumes.findById(candidateId);

        if(!candidate)
            throw notFound('Record Not Found.');

        const hashedPw = await bcrypt.hash(password, 12);
        let updateCandidate = new CandidateResumes({
            password: hashedPw
        });

        const result = await updateCandidate.save();

        if(result){
            const data = { message : 'Password Change Successfully' }
            return handleSuccessResponse(res, data);
        }

        throw badRequest('Some Error Occured');

    } catch(err){

        if (!err.status)
            err.status = 500;
            
        next(err);
    }

}

// exports.getCity = async(req, res, next) =>{

//     try{

//         let id = req.params.stateId;
//         const city = await City.find({state:id});
//         if(city) {
            
//             const data = { city : city }
//             return handleSuccessResponse(res, data);
//         }

//         throw badRequest('Some Error Occured'); 

//     }catch(err){
//         if (!err.status)
//             err.status = 500;
        
//         next(err);
//     }
// }