const bcrypt = require('bcryptjs');

const User = require('../models/user');
const {CandidateStatus} = require('../models/candidate_status');
const { Country, State, City } = require('../models/country');
var countries = require('../data/countries');
var states = require('../data/states');
var cities = require('../data/cities');

var async = require("async");

exports.welcome = async (req, res, next) => {
    try {
        res.status(200).json({message : 'Welcome to Node and Mongo Project'});
    } catch(err) {
        if (!err.statusCode)
            err.statusCode = 500;
        
        next(err);
    }
}

exports.buildData = async (req, res, next) => {
    try {
        /** add default record in user collection */
        const hashedPw = await bcrypt.hash('123456', 12);
        var user = new User({
            username: 'Admin',
            first_name : 'William',
            last_name : 'Mates',
            email : 'testing@gmail.com',
            password : hashedPw,
            is_deleted: '0'
        });

        await user.save();
        /** end of adding default record in user collection */
        
        res.status(200).json({message : 'Data Added Successfully'});
    } catch(err) {
        if (!err.statusCode)
            err.statusCode = 500;
        
        next(err);
    }
}

exports.buildData2 = async (req, res, next) => {
    try {
        /** add default record in user collection */

        var user = new CandidateStatus({
              name:'Cancelled',
        });

        await user.save();
        /** end of adding default record in user collection */
        
        res.status(200).json({message : 'Data Added Successfully 2'});
    } catch(err) {
        if (!err.statusCode)
            err.statusCode = 500;
        
        next(err);
    }
}
/** Country,state data */
// exports._saveStates = async(req, res, next) => {
//     var countries = await Country.find();
  
//     async.each(countries, function iteratee(country, nextCountry) {
  
//       console.log("==========Started " + country.name + "==============")
  
//       async.each(states, function iteratee(state, next) {
  
//         if (state.country_id == (country.id + '')) {
//           var st = new State({_id: state.id, name: state.name, country: country})
  
//           st.save(function (err, res) {
//             country
//               .states
//               .push(st)
//             country.save(function (er, resp) {
//               next()
//             })
//           })
  
//         } else {
//           next();
//         }
  
//       }, function () {
//         console.log("All States Done")
//         console.log("========== Ended " + country.name + "==============")
//       })
  
//     }, function () {
//       console.log("All Countries Done")
//     })
//   }
  
//   exports._saveCities = async(req, res, next) => {
//     var states = await State.find()
  
//     async.each(states, function iteratee(state, nextState) {
  
//       console.log("==========Started " + state.name + "==============")
  
//       async.each(cities, function iteratee(city, next) {
  
//         if (city.state_id == (state.id + '')) {
//           var ct = new City({_id: city.id, name: city.name, state: state})
  
//           ct.save(function (err, res) {
//             state
//               .cities
//               .push(ct)
//             state.save(function (er, resp) {
//               next()
//             })
//           })
  
//         } else {
//           next();
//         }
  
//       }, function () {
//         console.log("All Cities Done")
//         console.log("========== Ended " + state.name + "==============")
//       })
  
//     }, function () {
//       console.log("All States Done")
//     })
//   }
  
//   exports._saveCountries = async(req, res, next) => {
//     async.each(countries, function iteratee(country, next) {
//         var cn = new Country({_id: country.id, sortname: country.sortname, name: country.name})
  
//         cn.save(function (err, res) {
//           next();
//         })
//       }, function () {
//         console.log("================= All Countries loaded ===================");
//       })
//   }