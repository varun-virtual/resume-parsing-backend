const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {validationResult} = require('express-validator');
const _ = require('lodash')
const mongoose = require('mongoose');

const User = require('../models/user');
const UserRole = require('../models/UserRole')
const { notFound, handleSuccessResponse, badRequest, validationError, recordExists } = require('../utils/response-helper');
const { pagination, isObjectIdValid,userNameById,getCurrentUserRId, userRoleById, userRoleId } = require('../utils/helper');
const { SkillsMaster } = require("../models/skills_master");
const {CandidateResumes} = require('../models/candidate_resumes');
const { getCurrentUserAssignedUser } = require('../utils/helper');
const SkillsTempMaster = require("../models/skills_temp_master");



/**method to check whether login user is valid or not */
exports.login = async (req, res, next) => {
    try {
        let loadedUser;
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            throw validationError(errors.errors[0].msg)
        }

        let user;
        let isCandidateLogin = false
        const {email, password, loginFor, mobile } = req.body;
        
        if(loginFor === 'candidate'){
            
            user = await CandidateResumes.findOne(mobile === undefined ? { email: email, is_deleted:"0" } : { mobile, is_deleted:"0" });
            console.log('loginFor',loginFor,user);
            isCandidateLogin = true;
        }else{
            user = await User.findOne({ email: email, is_deleted:"0" });
        }
        console.log('loginFor',loginFor,user);
        if (!user) {
            throw notFound('Invalid Credentials, check email & password');
        }else{
            const isEqual = await bcrypt.compare(password, user.password);
            if (!isEqual) {
                throw notFound('Invalid Credentials, check email & password');
            }
        }
        
        loadedUser = user;

        const token = jwt.sign(
            {
                email: loadedUser.email,
                userId: loadedUser._id.toString(),
                user_role   : loginFor !== 'candidate' ? await getCurrentUserRId(loadedUser.user_role) : 'candidate'
            },
            'somesupersecretsecret',
            { expiresIn: '1h' }
        );
        //console.log("loadedUser ",loadedUser)
        const data = {
            user : {
                id          : loadedUser._id.toString(),
                username    : loginFor !== 'candidate' ? loadedUser.username : '',
                email       : loadedUser.email,
                full_name   : loginFor !== 'candidate' ? loadedUser.first_name + ' ' + loadedUser.last_name : loadedUser.name,
                last_name  : loginFor !== 'candidate' ? loadedUser.last_name : '',
                first_name  : loginFor !== 'candidate' ? loadedUser.first_name : '',
                user_role   : loginFor !== 'candidate' ? loadedUser.user_role : '',
                user_role_name:  loginFor !== 'candidate' ? await userRoleById(loadedUser.user_role) : 'Candidate',
                profile_image:  loginFor !== 'candidate' ? loadedUser.profile_image : '',
                role_id     : loginFor !== 'candidate' ? await userRoleId(loadedUser.user_role) : '',
                isCandidateLogin : isCandidateLogin
            },
            accessToken : token,
        }

        return handleSuccessResponse(res, data);
        

    } catch(err){
        
        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}

/**method to fetch all record */
exports.getUsers = async (req, res, next) => {
    try {
        let userId = req.userId;
        let userRole = req.userRole;

        let {page, search, sortingData} = req.query;
        const currentPage = parseInt(page) || 1;
        const perPage = parseInt(process.env.RECORDS_PER_PAGE);
        let finalData = [];
        let sortBy
        let orderBy 

        /** get current user role id */
        const roleId = await UserRole.find({ rid:userRole });
        /** to get assigned to user */
        const userids = await getCurrentUserAssignedUser(userId);
        // const query = { email: { $ne: 'testing@gmail.com' }, _id : { $in: userids } }
        const query = { _id : { $in: userids }, is_deleted : { $eq: '0' } }
        
        if (search) {
            query['$or'] = [ 
                { email: {'$regex' : search, '$options' : 'i' }}, 
                { username: {'$regex' : search, '$options' : 'i' }},
                { first_name: {'$regex' : search, '$options' : 'i'}},
                { last_name: {'$regex' : search, '$options' : 'i'}},
            ]
        }
        const totalItems = await User.find(query).countDocuments();
        
        const sorting = JSON.parse(sortingData)
        let sortObject = {}
        if(_.isEmpty(sorting)){
            sortBy = '_id'
            orderBy = -1
        }else{
            sortBy = sorting.name
            orderBy = sorting.order === 'asc' ? 1 : -1
            
        }
        sortObject[sortBy] = orderBy
        // const userData = await User.find(query)
        //                             .sort({_id : -1})
        //                             //.populate('role_id') //required when related data is needed
        //                             .skip((currentPage - 1) * perPage).limit(perPage);
        
        const userData = await User.aggregate( [
            {
                $lookup:
                  {
                    from: 'candidate_resumes',
                    localField: "_id", 
                    foreignField: "created_by", as: "resumeCount",
                    
                  }
             },
             {
                $lookup: {
                    from: "user_roles", // collection to join
                    localField: "user_role", //field from the input documents
                    foreignField: "_id", //field from the documents of the "from" collection
                    as: "userrole" // output array field
                 }
             },
            // { $match: { email: { $not: { $eq: 'testing@gmail.com' } } } }
            { $match: query  }
            ] ).sort(sortObject)
              //  .populate('role_id') //required when related data is needed
                .skip((currentPage - 1) * perPage).limit(perPage);                                    

        
        if(userData){
            finalData = userData.map(obj => {
                let role_name=''
                if(obj.userrole.length){
                      role_name=obj.userrole[0] 
                 }    
                /**count object length */
                var count = Object.keys(obj.resumeCount).length;
                return {
                    "id"            : obj._id,
                    "username"      : obj.username,
                    "first_name"    : obj.first_name,
                    "last_name"     : obj.last_name,
                    "email"         : obj.email,
                    "user_role"     : role_name.name,
                    "resumeCount"   : count,
                }
            });
                                  

            const paginationData = pagination(currentPage, totalItems, 'user');

            const data = {
                user    : finalData,
                userRole: userRole,
                ...paginationData
            }
            return handleSuccessResponse(res, data)
        }
        
        throw notFound('No Records Found')

    } catch(err){
        console.log(err)
        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}

/**method for saving a new record */
exports.saveUser = async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            throw validationError(errors.errors)
        }

        const {username, email, first_name, last_name, password, user_role, assigned_to, is_deleted} = req.body;
       
        //Check email is unique for new user
        const userExists = await User.findOne({ email: email });
        if (userExists)
            throw recordExists('A user with this email already exists.');

        //Check username is unique for new user
        const usernameExists = await User.findOne({ username: username });
        if (usernameExists)
            throw recordExists('A user with this username already exists.');

        const hashedPw = await bcrypt.hash(password, 12);
        
        var user = new User({
            username,
            first_name,
            last_name,
            email,
            password : hashedPw,
            created_by : req.userId, //fetched from middleware
            user_role,
            assigned_to,
            is_deleted
        });
        
        const result = await user.save();
        if(result){
            //sendEmail(); //send email

            return handleSuccessResponse(res, { data : 'User Created Successfully' });
        }
        
        throw badRequest('Some Error Occured');

    } catch(err){
        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}

/**method to fetch a single record */
exports.getUser = async (req, res, next) => {
    try {
        const id = req.params.userId;
        if(!isObjectIdValid(id))
            throw notFound('Record Not Found.');

        let finalData = [];
        const userData = await User.findById(id);
       
        if(userData){
         
            finalData = {
                "id"            : userData._id,
                "username"      : userData.username,
                "first_name"    : userData.first_name,
                "last_name"     : userData.last_name,
                "email"         : userData.email,
                "user_role"     : userData.user_role,
                "assigned_to"   : userData.assigned_to,
                "assigned_name" : await userNameById(userData.assigned_to)
            }
            const userRoles = await UserRole.find();
            const data = { user : finalData, user_roles: userRoles }

            return handleSuccessResponse(res, data);
        }
        
        throw notFound('No Record Found');

    } catch(err){
        if (!err.status)
            err.status = 500;

        next(err);
    }
}

/**method to update an existing record */
exports.updateUser = async (req, res, next) => {
    try{
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            throw validationError(errors.errors)
        }
        
        const userId = req.params.userId;
        if(!isObjectIdValid(userId))
            throw notFound('Record Not Found.');

        const {username, email, first_name, last_name, user_role,assigned_to } = req.body;
        
        const user = await User.findById(userId);
        if(!user)
            throw notFound('No Records Found');
        
        user.username   = username;
        user.email      = email;
        user.first_name = first_name;
        user.last_name  = last_name;
        user.user_role  = user_role;
        user.assigned_to  = assigned_to;
        const result    = await user.save();

        if(result) {
            const data = { data : 'User Updated Successfully' }
            return handleSuccessResponse(res, data);
        }
            
        throw badRequest('Some Error Occured');

    } catch(err){
        if (!err.status)
            err.status = 500;
        
        next(err);
    }
}

/**method to delete record */
exports.deleteUser = async (req, res, next) => {
    try {
        const id = req.params.userId;
        if(!isObjectIdValid(id))
            throw notFound('Record Not Found.');

        // const user = await User.findByIdAndRemove(id);
        const user = await User.findById(id);
        user.is_deleted  = "1";
        const result    = await user.save();
        
        if(result){
            const data = {
                data : 'User Deleted Successfully'
            }
            return handleSuccessResponse(res, data);
        }
        
        throw badRequest('Some Error Occured');

    } catch(err){
        if (!err.status)
            err.status = 500;

        next(err);
    }
}


/**method to all skills record */
exports.getSkills = async (req, res, next) => {
    try {
        let {page, search, sortingData } = req.query;
        const currentPage = parseInt(page) || 1;
        const perPage = parseInt(process.env.RECORDS_PER_PAGE);
        let finalData = [];
        let sortBy
        let orderBy 
        // const query = { value: { $ne: 'testing@gmail.com' } };
        const query = { is_deleted : { $eq: '0' } };
        
        if (search) {
            query['$or'] = [
                { value: {'$regex' : search, '$options' : 'i'}}
            ]
        }

        const totalItems = await SkillsMaster.find(query).countDocuments();
        const sorting = JSON.parse(sortingData)
        let sortObject = {}
        if(_.isEmpty(sorting)){
            sortBy = '_id'
            orderBy = -1
        }else{
            sortBy = sorting.name
            orderBy = sorting.order === 'asc' ? 1 : -1
            
        }
        sortObject[sortBy] = orderBy
        const userData = await SkillsMaster.find(query)
                                    .sort(sortObject)
                                    //.populate('role_id') //required when related data is needed
                                    .skip((currentPage - 1) * perPage).limit(perPage);
        let srno = 0;                            
        if(currentPage===1){
            srno = 0
        }else{
            srno = (currentPage-1)*10
        }
        if(userData){
            finalData = userData.map(obj => {
                srno = srno+1
                return {
                    "id"         : obj._id,
                    "value"      : obj.value,
                    "serial_no"  : srno
                    
                }
            });

            const paginationData = pagination(currentPage, totalItems, 'skills');

            const data = {
                skills    : finalData,
                ...paginationData
            }
            return handleSuccessResponse(res, data)
        }
        
        throw notFound('No Records Found')

    } catch(err){
        console.log(err)
        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}

/**method for saving a new record */
exports.saveSkills = async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            throw validationError(errors.errors)
        }

        const { skills_name } = req.body;
        
        const userExists = await SkillsMaster.findOne({ value: skills_name, is_deleted:'0' });
        if (userExists)
            throw recordExists('Skill is already exists.');

        var skill = new SkillsMaster({
            
            value : skills_name,
            label : skills_name,
            is_deleted : "0"
        });
        
        const result = await skill.save();
        if(result){
            //sendEmail(); //send email

            return handleSuccessResponse(res, { data : 'Skill Created Successfully' });
        }
        
        throw badRequest('Some Error Occured');

    } catch(err){
        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}

/**method to fetch a single record */
exports.getSkill = async (req, res, next) => {
    try {
        const id = req.params.skillId;
        if(!isObjectIdValid(id))
            throw notFound('Record Not Found.');

        let finalData = [];
        const skillData = await SkillsMaster.findById(id);
        if(skillData){
            finalData = {
                "id"            : skillData._id,
                "skills_name"      : skillData.value,
                }

            const data = { skills : finalData }

            return handleSuccessResponse(res, data);
        }
        
        throw notFound('No Record Found');

    } catch(err){
        if (!err.status)
            err.status = 500;

        next(err);
    }
}

exports.getUserByRole=async(req,res,next)=>{
    try{
        const id = req.params.userId;
        let data={}
        if(!isObjectIdValid(id))
            throw notFound('Record Not Found.');

        let finalData = [];
        const userRole = await UserRole.findById(id);
        if(userRole){
            let main_role_id=userRole.rid
            let allRoleKeyList=[]
            for(let i = main_role_id; i>0; i-- ){
                  allRoleKeyList.push(parseInt(i))
            }
            if(allRoleKeyList){
                const roleListId = await UserRole.find({ "rid" : { $in : allRoleKeyList } });
                if(roleListId){
                    let roleListArray=[]
                    roleListId.forEach(data => {
                          roleListArray.push(data._id)
                    });
                    if(roleListArray){
                        const sendResponce={};
                        const userListData = await User.find({ "user_role" : { $in : roleListArray } });
                        if(userListData){
                            finalData = userListData.map(obj => {
                                  return{
                                     id:obj._id,
                                     name:obj.username
                                  }
                            })
                              
                        }
                    }
                }
            }
            return handleSuccessResponse(res, finalData);   
        }
        
        throw notFound('No Record Found');

    }
    catch(err){
        if (!err.status)
        err.status = 500;
    
        next(err);
    }
}

/**method to update an existing record */
exports.updateSkill = async (req, res, next) => {
    try{
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            throw validationError(errors.errors)
        }
        
        const skillId = req.params.skillId;
        if(!isObjectIdValid(skillId))
            throw notFound('Record Not Found.');

        const {skills_name} = req.body;
        
        const skills = await SkillsMaster.findById(skillId);
        if(!skills)
            throw notFound('No Records Found');
        
            skills.value   = skills_name;
            skills.label   = skills_name;
            skills.is_deleted   = "0";
       
        const result    = await skills.save();

        if(result) {
            const data = { data : 'Skill Updated Successfully' }
            return handleSuccessResponse(res, data);
        }
            
        throw badRequest('Some Error Occured');

    } catch(err){
        if (!err.status)
            err.status = 500;
        
        next(err);
    }
}

/**method to delete record */
exports.deleteSkill = async (req, res, next) => {
    try {
        const is_del = '1';
        const id = req.params.skillId;
        if(!isObjectIdValid(id))
            throw notFound('Record Not Found.');

        // const skill = await SkillsMaster.findByIdAndRemove(id);
        const skill = await SkillsMaster.findById(id);
        skill.is_deleted  = is_del;
        const result    = await skill.save();

        if(result){
            const data = {
                data : 'Skill Deleted Successfully'
            }
            return handleSuccessResponse(res, data);
        }
        

    } catch(err){
        if (!err.status)
            err.status = 500;

        next(err);
    }
}

/**
 * Get user roles
 */
exports.getUserRoles = async (req, res, next) => {
    try {
        const userRoles = await UserRole.find();
        const data = { user_roles: userRoles }
        return handleSuccessResponse(res, data);
    } catch (err) {
        if (!err.status)
            err.status = 500;

        next(err);
    }
}

/**method to all temporary skills record */
exports.getTempSkills = async (req, res, next) => {
    try {
        let {page, search, sortingData } = req.query;
        const currentPage = parseInt(page) || 1;
        const perPage = parseInt(process.env.RECORDS_PER_PAGE);
        let finalData = [];
        let sortBy
        let orderBy 
        const query = { };
        if (search) {
            query['$or'] = [
                { value: {'$regex' : search, '$options' : 'i'}}
            ]
        }

        const totalItems = await SkillsTempMaster.find(query).countDocuments();
        const sorting = JSON.parse(sortingData)
        let sortObject = {}
        if(_.isEmpty(sorting)){
            sortBy = '_id'
            orderBy = -1
        }else{
            sortBy = sorting.name
            orderBy = sorting.order === 'asc' ? 1 : -1 
        }
        sortObject[sortBy] = orderBy
        const skillData = await SkillsTempMaster.find(query)
                                    .sort(sortObject)
                                    .skip((currentPage - 1) * perPage).limit(perPage);
        let srno = 0;                            
        if(currentPage===1){
            srno = 0
        }else{
            srno = (currentPage-1)*10
        }
        if(skillData){
            finalData = skillData.map(obj => {
                srno = srno+1
                return {
                    "id"         : obj._id,
                    "value"      : obj.value,
                    "serial_no"  : srno
                    
                }
            });

            const paginationData = pagination(currentPage, totalItems, 'skills');

            const data = {
                skillsTemp    : finalData,
                ...paginationData
            }
            return handleSuccessResponse(res, data)
        }
        
        throw notFound('No Records Found')

    } catch(err){
        console.log(err)
        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}

exports.updateTempSkills = async(req, res, next) =>{
    
    try{
       
        const { skills_status, value } = req.body;
        const id = req.params.skillId;
        var skillObjectId = mongoose.Types.ObjectId(id); 
        let isUpdate = false;
        if(!isObjectIdValid(id))
            throw notFound('Record Not Found.');

        if(skills_status=== 'true'){
            const re = new RegExp(value.trim(), 'i');
            const isExists = await SkillsMaster.find({value:re}).countDocuments();
            if(isExists===0){
                
                const skill = await SkillsMaster();
                skill.value  = value;
                skill.label  = value
                // skill.is_deleted  = 0
                await skill.save();
                
            }else{
                isUpdate = true;
            }
        }
        const result = await SkillsTempMaster.deleteOne({ _id: skillObjectId });
        if(result){
            
            if(isUpdate){
                var msg = 'Skill Already Exists';
            }else{    
                var msg = 'Skill Status Changes Successfully';
            }
            const data = {
                data : msg
            }
            return handleSuccessResponse(res, data);
        } 
    } catch(err){
        
        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}