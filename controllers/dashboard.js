const { CandidateStatus } = require("../models/candidate_status");
const {CandidateResumes} = require('../models/candidate_resumes');
const SkillsTempMaster = require("../models/skills_temp_master");
const { SkillsMaster } = require("../models/skills_master");
const { notFound, handleSuccessResponse, badRequest, validationError, recordExists } = require('../utils/response-helper');
const { getCurrentUserAssignedUser  } = require('../utils/helper');
const _ = require('lodash');
const mongoose = require('mongoose');

/**method to count resume record */
exports.dashboardData = async (req, res, next) => {
    try {
        let finalData = [];
        let userId = req.userId ? req.userId :''
        
        // await SkillsMaster.find({ value:  });
        const subuserids=await getCurrentUserAssignedUser(userId);
        //console.log(subuserids)
        var today = new Date();
        let startDate = Date.now();
        var weekEndDate = Date.parse(new Date(today.getFullYear(), today.getMonth(), today.getDate()-7));
        var monthlyEndDate = Date.parse(new Date(today.getFullYear(), today.getMonth(), today.getDate()-30));

        const weeklyQuerysubuser = {  
            resumeStartTimestamp:{ 
                             $lt : startDate,
                             $gte : weekEndDate
                             },
                             created_by: {$in: subuserids},
                             is_deleted:'0'
              };
        const monthlyQuerysubuser = {  
                    resumeStartTimestamp:{ 
                                    $lt : startDate,
                                    $gte : monthlyEndDate
                                    },
                                    created_by: {$in: subuserids},
                                    is_deleted:'0'
                    };             
        const weeklyResumessubuser = await CandidateResumes.find(weeklyQuerysubuser).countDocuments();
        const totalResumessubuser = await CandidateResumes.find({created_by: {$in: subuserids},is_deleted:'0'}).countDocuments();
        const monthlyResumessubuser = await CandidateResumes.find(monthlyQuerysubuser).countDocuments();

        
        const weeklyQuery = {  
                   resumeStartTimestamp:{ 
                                    $lt : startDate,
                                    $gte : weekEndDate
                                    },
                                    created_by: {$eq: userId},
                                    is_deleted:'0'
                     };
        const monthlyQuery = {  
                   resumeStartTimestamp:{ 
                                    $lt : startDate,
                                    $gte : monthlyEndDate
                                    },
                                    created_by: {$eq: userId},
                                    is_deleted:'0'
                     };             
        const weeklyResumes = await CandidateResumes.find(weeklyQuery).countDocuments();
        const totalResumes = await CandidateResumes.find({created_by: userId,is_deleted:'0'}).countDocuments();
        const monthlyResumes = await CandidateResumes.find(monthlyQuery).countDocuments();

        /** get current user assigned to user */
        const userids = await getCurrentUserAssignedUser(userId);
        var UserObjectId = mongoose.Types.ObjectId(userId); 
        userids.push(UserObjectId);
        
        /** get Status Graph Data */
        const status =  await CandidateStatus.find({});
        let statusCandidateData = [];
        let statusData = [];
        
        for(let i = 0 ; i<=status.length-1; i++){
            
            statusData.push(status[i].name);
            const resume_status = await CandidateResumes.find({candidate_status:status[i].pid, is_deleted : "0", created_by: {$in: userids} }).count();
            if(resume_status == 0)
                statusCandidateData.push(0);
            else
                statusCandidateData.push(resume_status);   
        }


        /** get latest five canditate list */
        let userlists;
        if(!_.isEmpty(userids)){
            userlists = { "created_by": { $in: userids },"is_deleted" : "0" }
        }else{
            userlists = { is_deleted : "0" }
        }
        
        const user_resume = await CandidateResumes.aggregate([
            {
                $match: userlists 
            },
            {
                $lookup: {
                    from: "candidate_status", 
                    localField: "candidate_status", 
                    foreignField: "pid", 
                    as: "candidate_status_name" 
                 }
             },
             {
                "$project": {
                    "_id": 0,
                    "candidate_id": 1,
                    "candidate_status":1,
                    "email": 1,
                    "phone": 1,
                    "name": 1,
                    "candidate_status_name":1,
                    "created_at": { $dateToString: { format: "%d-%m-%Y", date: "$created_at" } },
                }
             },
             { $sort: { created_at : -1} },
             { $limit:5}
            ]);

        // if(totalResumes){
            const data={
                weeklyResumes: weeklyResumes,
                totalResumes,
                monthlyResumes,
                weeklyResumessubuser,
                totalResumessubuser,
                monthlyResumessubuser,
                statusList : statusData, 
                statusData : statusCandidateData,
                topResume  : user_resume    
             }
             //console.log(data);
            return handleSuccessResponse(res, data)
        //  }
        
        // throw notFound('No Records Found')

    } catch(err){
        console.log(err);
        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}



