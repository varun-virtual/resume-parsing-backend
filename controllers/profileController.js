const bcrypt = require('bcryptjs');
const  User = require('../models/user');
const {validationResult} = require('express-validator');
const { notFound, handleSuccessResponse, badRequest, validationError,  recordExists } = require('../utils/response-helper');
const {  isObjectIdValid } = require('../utils/helper');

exports.updateProfile = async(req, res, next) => {

    try{
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            throw validationError(errors.errors)
        }
        //console.log('file',req.file)
        const userId = req.params.profileId;
        if(!isObjectIdValid(userId))
            throw notFound('Record Not Found.');

        const {first_name, last_name } = req.body;
        
        //console.log('userId',userId)
        const user = await User.findById(userId);
        if(!user)
            throw notFound('No Records Found');
        user.first_name = first_name;
        user.last_name  = last_name;
        if(req.file)
            user.profile_image  = req.file.filename;
        const result    = await user.save();
        //console.log("result ",result,req.file);
        if(result) {
            const data = { data : 'User Updated Successfully' }
            return handleSuccessResponse(res, data);
        }
            
        throw badRequest('Some Error Occured');

    } catch(err){
        if (!err.status)
            err.status = 500;
        
        next(err);
    }

}


exports.changePassword = async (req, res, next) => {
    //console.log("req.bodys ",req.body)
    
    try {

        const {password, old_password } = req.body;
        let profileId = req.params.profileId; 
        const user = await User.findById(profileId);

        if (!user) {
            throw notFound('Record Not Found.');
        }else{
           
            const isEqual = await bcrypt.compare(old_password, user.password);
            
            if (!isEqual) {
                throw notFound('Invalid old password');
            }else{
                const hashedPw = await bcrypt.hash(password, 12);
                user.password = hashedPw
                const result = await user.save();
                if(result){
                    const data = { message : 'Password Change Successfully' }
                    return handleSuccessResponse(res, data);
                }
                throw badRequest('Some Error Occured');
            }
        }
    } catch(err){

        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}