const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Candidate_Status = new Schema(
    {
        name: {
            type: String,
        },
        pid: {
            type: String,
        }
    }
);

const CandidateStatus = mongoose.model("candidate_status", Candidate_Status,"candidate_status");

module.exports = { CandidateStatus };