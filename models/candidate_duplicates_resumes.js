const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Candidate_Duplicate = new Schema(
    {
        duplicates: {
            type: String,
        },
    },
    {timestamps:true}
);

const CandidateDuplicates= mongoose.model("candidate_duplicates_resumes", Candidate_Duplicate,"candidate_duplicates_resumes");

module.exports = { CandidateDuplicates };