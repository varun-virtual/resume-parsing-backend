const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const userRoleSchema = new Schema({
    name: {
        type: String
    },
    rid:{
        type:String
    }
});

const UserRole = mongoose.model("user_roles", userRoleSchema);

module.exports = UserRole;