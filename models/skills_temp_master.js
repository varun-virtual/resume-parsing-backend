const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const opts = {
  timestamps: {
    createdAt: "created_at",
    updatedAt: "updated_at",
  },
};
const Skills_Temporary_Data = new Schema(
  {
    value: {
      type: String,
    }
  },
  opts
);

const SkillsTempMaster = mongoose.model("skills_temporary_master", Skills_Temporary_Data);

module.exports =  SkillsTempMaster;
