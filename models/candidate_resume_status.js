const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const resumeStatusSchema = new Schema({
    candidate_id : {
        type: mongoose.Schema.Types.ObjectId,
        ref : 'candidate_resumes'
    },
    candidate_status: {
        type: String,
    },
    note: {
        type: String,
    },
    user_id : {
        type: mongoose.Schema.Types.ObjectId,
        ref : 'User'
    },
    created_at: {
        type: Number,
		default: Date.now(),
    }
});

const CandidateResumeStatus = mongoose.model("candidate_resume_status", resumeStatusSchema);

module.exports =  CandidateResumeStatus;
