const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var nDate = Date.now();
const opts = {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  };
const resumeSchema = new Schema({
    name: {
        type: String,
        default:''
    },
    email: {
        type: String,
        default:''
     //   required: true
    },
    phone: {
        type: String,
        default:''
      //  required: true
    },
    objective:{
        type : String,
        default:''
    },
    resumePath:{
        type : String,
        default:''
    },
    candidate_status:{
        type : String,
        default:''
    },
    totalYearsExperience : {
        type : Object,
        default:''
    },
    resumeStartTimestamp: {
        type: Number,
		default: nDate,
    },
    place:{},
    skills:[],
    degree:[],
    designition:[],
    education:[],
    university:[],
    workExperience:[],
    raw_resume:[],
    dob:{
        type : String,
        default:''
    },
    address:{
        type : String,
        default:''
    },
    designation:{
        type : String,
        default:''
    },
    current_ctc:{
        type : String,
        default:''
    },
    expected_ctc:{
        type : String,
        default:''
    },
    resume_label:{
        type : String,
        default:''
    },
    total_experience:{
        type : Number,
        default:0
    },
    country:{
        type : Number,
        default:0
    },
    state:{
        type : Number,
        default:0
    },
    zip:{
        type : String,
        default:''
    },
    created_by : {
        type: mongoose.Schema.Types.ObjectId,
        ref : 'User'
    },
    is_deleted: {
        type: String,
        ref : 'is_deleted',
        default:0
    },
    password: {
        type: String,
        trim: true,
        required: true,
        validate(value){
            if(value.toLowerCase().includes('password')){
                
                throw new Error('Password cannot be password')
            }
        }
    }

    
},opts);

// resumeSchema.methods.generateAuthToken = function () {
//     const token = jwt.sign(
//         { _id: this._id, name: this.name },
//         process.env.JWTPRIVATEKEY
//     );
//     return token;
// };

const CandidateResumes = mongoose.model("candidate_resumes", resumeSchema);

// const validate = (resume) => {
//     const schema = Joi.object({
//         name: Joi.string().required(),
//         email: Joi.string().email().required(),
//         phone: Joi.string().required(),
//     });
//     return schema.validate(resume);
// };

module.exports = { CandidateResumes };
