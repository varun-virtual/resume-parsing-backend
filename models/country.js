const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const citySchema = new Schema({
    _id: {
      type: Number
    },
    name: {
      type: String
    },
    state: {
      type: Number,
      // ref: 'state'
    }
  });
  
  const stateSchema = new Schema({
    _id: {
      type: Number
    },
    name: {
      type: String
    },
    cities: [
      {
        type: Schema.Types.ObjectId,
        ref: 'city'
      }
    ],
    country: {
      type: Number,
      // ref: 'country'
    }
  });
  
  const countrySchema = new Schema({
    _id: {
      type: Number
    },
    sortname: {
      type: String
    },
    name: {
      type: String
    },
    states: [
      {
        type: Schema.Types.ObjectId,
        ref: 'state'
      }
    ]
  });

  var Country = mongoose.model('country', countrySchema);
var State = mongoose.model('state', stateSchema);
var City = mongoose.model('city', citySchema);

module.exports = { Country, State, City };