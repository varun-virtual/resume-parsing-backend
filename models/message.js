const mongoose =  require('mongoose');
const Schema = mongoose.Schema;

const Message = new Schema({
    message_text:{
        type : String,
        required : true
    },
    message_from:{
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    },
    message_to:{
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    },
    is_view:{
        type : Boolean,
        default : false
    }
});