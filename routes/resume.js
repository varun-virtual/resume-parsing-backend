const express = require("express");
const router = express.Router();
const multer = require('multer');
const path = require('path');
const { body} = require('express-validator');

const ResumeController = require('../controllers/Resume');
const isAuth = require('../middleware/auth');
const {  handleSuccessResponse} = require('../utils/response-helper');
const parseIt = require('../src/parseIt');


// handle storage using multer
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads');
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`);
    }
});
var upload = multer({ storage: storage });

// handle multiple file upload
router.post('/', upload.array('dataFiles', 10), async (req, res, next) => {
    const files = req.files;
    let result = true;
    if (!files || (files && files.length === 0)) {
        return res.status(400).send({ message: 'Please upload a file.' });
    }
    // let path_upload = 'uploads/' + files[0].filename;
    // console.log('1');
    // result = await parseIt.resumeCheck(path_upload);
    // console.log('2');
    // if(result != undefined){
    //     if(result){
    //         return res.send({ status: true, message: 'File uploaded successfully.', files });
    //     }
    //     else{
    //         return res.send({ status: false, message: files[0].originalname + ' is already exist', files });
    //     }
    // }else{
    //     return res.send({ status: true, message: 'File uploaded successfully.', files });
    // }

    return res.send({ message: 'File uploaded successfully.', files });
    //return handleSuccessResponse(res, { data : 'File uploaded successfully', files });
    
});


router.get('/view/:filename', ResumeController.getFile)

router.get('/', isAuth, ResumeController.getResumes); //fetch all user
router.get('/candidate/:candidateId',  ResumeController.getCandidate); //fetch particular candidate

router.put('/candidate/:candidateId', upload.single('document'), isAuth,
[
    body('name').trim().exists().withMessage('Please enter name'),
    body('skills').trim().exists().withMessage('Please enter skills'),
    body('place').trim().exists().withMessage('Please enter city'),
    body('workExperience').trim().exists().withMessage('Please enter company'),
    body('phone').trim().exists().withMessage('Please enter phone'),
    body('email').isEmail().normalizeEmail().withMessage('Please enter valid email.'),
], ResumeController.updateCandidate); // update single candidate isAuth,

router.post("/parse", isAuth, ResumeController.parseFile);

router.post("/manual", upload.single('document'), isAuth, ResumeController.saveManualResume);

router.delete('/resume/:resumeId', isAuth ,ResumeController.deleteResume); //delete resume 

// router.get("/dashboard", ResumeController.countResumes);

router.post("/delete-file", ResumeController.removeFile);

router.post("/update-status", isAuth,ResumeController.updateStatusField);

router.get("/communication/:candidateId",ResumeController.candidateCommunication);

router.post("/communication-comment", isAuth,ResumeController.addCandidateComment);

router.get("/country",ResumeController.getCountry);

router.get("/state/:countryId",ResumeController.getState);

router.post("/candidate/changePassword",ResumeController.changePassword);


// router.get("/city/:stateId",ResumeController.getCity);

// router.get("/communication/:candidateId",ResumeController.candidateCommunication);

// router.get("/communication/:candidateId",ResumeController.candidateCommunication);

module.exports = router;
