const express = require('express');
const router = express.Router();

const dependantDataController = require('../controllers/dependantData');

router.get('/',dependantDataController.welcome);
router.get('/buildData',dependantDataController.buildData);
router.get('/buildData2',dependantDataController.buildData2);
/** Country,state data */
// router.get('/country-data',dependantDataController._saveCountries);
// router.get('/state-data',dependantDataController._saveStates);
// router.get('/city-data',dependantDataController._saveCities);

module.exports = router;