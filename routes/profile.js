const express = require('express');
const router = express.Router();
const { body} = require('express-validator');
const profileController = require("../controllers/profileController");
const multer = require('multer');
const path = require('path');
const isAuth = require('../middleware/auth');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'profile_images');
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`);
    }
});
var upload = multer({ storage: storage });

router.put('/change-password/:profileId', isAuth , profileController.changePassword);

router.put('/edit/:profileId', isAuth , upload.single('profile_image'), profileController.updateProfile); // update single candidate isAuth,

module.exports = router;
