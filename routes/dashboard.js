const express = require("express");
const router = express.Router();
const multer = require('multer');
const path = require('path');
const { body} = require('express-validator');

const Dashboardontroller = require('../controllers/dashboard');
const isAuth = require('../middleware/auth');

router.get('/', isAuth, Dashboardontroller.dashboardData); 



module.exports = router;
