var request = require("request");
var cheerio = require("cheerio");
var _ = require('lodash');

module.exports = {
  titles: {
    objective: ['objective','career','career objective'],
    contact: ['personal information','personal details','personal detail'],
    summary: ['professional summary','summary','description','profile summary','personal profile and competencies','personal profile & competencies','personal profile'],
    technology: ['technology', 'technologies','core competencies'],
    workExperience: ['employment scan','experience','work experience','expertise','experience and exposure','experience & exposure','experience summary', 'Professional Training and Certification'],
    professional_experience:['professional experience','professional work experience'],
    technical_experience:['technical expertise'],
    education: ['educational qualifications','educational qualification','studies','qualifications','education','academic details','education summary','experience detail','academic qualification','special qualification'],
    skills: ['skills','technical skill', 'Skills & Expertise','it/computer skills','programming skills','it skills','key skills','technical skills','technical summary','professional skills','documentation tools','tools'],
    languages: ['languages','programming_languages','programming_language','programming languages'],
    projects: ['projects','project details','project undertaken','project summary','academics project'],
    links: ['links'],
    place:['city','state','place'],
    district:['district'],
    pincode:['pin code'],
    date: ['date','signature'],
    positions: ['positions', 'position'],
    responsibilities: ['roles and responsibilities','Roles and Responsibilities','Roles & Responsibilities','responsibilities'],
    profiles: ['profiles', 'social connect', 'social-profiles', 'social profiles'],
    awards: ['awards','awards/certificates','awards & certificates','certifications and independent coursework','certifications & independent coursework','certification course','certifications and achievements','certifications &   achievements','certificate','certifications','awards and recognitions'],
    honors: ['honors'],
    additional: ['additional','email','e-mail','Mob No','Mobile Number','phone'],
    certification: ['certification', 'certifications'],
    interests: ['interests','personal skills and hobbies','extracurricular activities'],
    activities:['activities'],
    training:['training','training / courses','training and courses','training & courses']
  },
  profiles: [
    ['github.com', function(url, Resume, profilesWatcher) {
      download(url, function(data, err) {
        if (data) {
          var $ = cheerio.load(data),
            fullName = $('.vcard-fullname').text(),
            location = $('.octicon-location').parent().text(),
            mail = $('.octicon-mail').parent().text(),
            link = $('.octicon-link').parent().text(),
            clock = $('.octicon-clock').parent().text(),
            company = $('.octicon-organization').parent().text();

          Resume.addObject('github', {
            name: fullName,
            location: location,
            email: mail,
            link: link,
            joined: clock,
            company: company
          });
        } else {
          return console.log(err);
        }
        //profilesInProgress--;
        profilesWatcher.inProgress--;
      });
    }],
    ['linkedin.com', function(url, Resume, profilesWatcher) {
      download(url, function(data, err) {
        if (data) {
          var $ = cheerio.load(data),
            linkedData = {
              positions: {
                past: [],
                current: {}
              },
              languages: [],
              skills: [],
              educations: [],
              volunteering: [],
              volunteeringOpportunities: []
            },
            $pastPositions = $('.past-position'),
            $currentPosition = $('.current-position'),
            $languages = $('#languages-view .section-item > h4 > span'),
            $skills = $('.skills-section .skill-pill .endorse-item-name-text'),
            $educations = $('.education'),
            $volunteeringListing = $('ul.volunteering-listing > li'),
            $volunteeringOpportunities = $('ul.volunteering-opportunities > li');

          linkedData.summary = $('#summary-item .summary').text();
          linkedData.name = $('.full-name').text();
          // current position
          linkedData.positions.current = {
            title: $currentPosition.find('header > h4').text(),
            company: $currentPosition.find('header > h5').text(),
            description: $currentPosition.find('p.description').text(),
            period: $currentPosition.find('.experience-date-locale').text()
          };
          // past positions
          _.forEach($pastPositions, function(pastPosition) {
            var $pastPosition = $(pastPosition);
            linkedData.positions.past.push({
              title: $pastPosition.find('header > h4').text(),
              company: $pastPosition.find('header > h5').text(),
              description: $pastPosition.find('p.description').text(),
              period: $pastPosition.find('.experience-date-locale').text()
            });
          });
          _.forEach($languages, function(language) {
            linkedData.languages.push($(language).text());
          });
          _.forEach($skills, function(skill) {
            linkedData.skills.push($(skill).text());
          });
          _.forEach($educations, function(education) {
            var $education = $(education);
            linkedData.educations.push({
              title: $education.find('header > h4').text(),
              major: $education.find('header > h5').text(),
              date: $education.find('.education-date').text()
            });
          });
          _.forEach($volunteeringListing, function(volunteering) {
            linkedData.volunteering.push($(volunteering).text());
          });
          _.forEach($volunteeringOpportunities, function(volunteering) {
            linkedData.volunteeringOpportunities.push($(volunteering).text());
          });

          Resume.addObject('linkedin', linkedData);
        } else {
          return console.log(err);
        }
        profilesWatcher.inProgress--;
      });
    }],
    'facebook.com',
    'bitbucket.org',
    'stackoverflow.com'
  ],
  inline: {
    //address: 'address',
    skype: 'skype'
  },
  regular: {
    name: [
      /(\b(?!Name\b)(?!Resume\b)(?!RESUME\b)(?!Career\b)(?!Objective\b)\b([A-Z][a-z]*)(\s[A-Z][a-z]*)?(\s(?!(Address|Phone|Present|Academic|Python)\b)[A-Z][a-z]*)|(?!RESUME\b)\b(?!\s)[A-Z]*(\s[A-Z][A-Z]*)?(\s[A-Z][A-Z]*)|(?!RESUME\b)\b[A-Z](\s[A-Z][A-Z]*)?(\s[A-Z][A-Z]*)|(?!Name\b)(?!Resume\b)(?!RESUME\b)(?!\s)\b([\w\-]+))/
    ],
    email: [
      /((?!id\b)\b(?!-)[A-Za-z0-9_\.-]+)@([\dA-Za-z\.-]+)\.([A-Za-z\.]{2,6})/
    ],
    phone: [
      /(([+]?[0-9]{2}?[-]?[\s]?[0-9]{10,12})|([0-9]{10,12})|((?:\+?\d{1,3}[\s-])?\(?\d{2,3}\)?[\s.-]?\d{3}[\s.-]\d{4,5}))/
    ]
  }
};

// helper method
function download(url, callback) {
  request(url, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      callback(body);
    } else {
      callback(null, error)
    }
  });
}