let ParseBoy = require('../src/ParseBoy');
let processing = require('../src/libs/processing');
const { CandidateResumes } = require("../models/candidate_resumes");
const CandidateResumeStatus = require("../models/candidate_resume_status");
const SkillsTempMaster = require("../models/skills_temp_master");
const { checkSkillsData  } = require('../utils/helper');
let logger = require('tracer').colorConsole();
const bcrypt = require('bcryptjs');

const { sendMail } = require('../utils/helper');

RegExp.escape = function(s) {
  return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};

//function date difference
function dateDiff(startingDate, endingDate) {
  var startDate = new Date(new Date(startingDate).toISOString().substr(0, 10));
  if (!endingDate) {
      endingDate = new Date().toISOString().substr(0, 10);    // need date in YYYY-MM-DD format
  }
  var endDate = new Date(endingDate);
  if (startDate > endDate) {
      var swap = startDate;
      startDate = endDate;
      endDate = swap;
  }
  var startYear = startDate.getFullYear();
  var february = (startYear % 4 === 0 && startYear % 100 !== 0) || startYear % 400 === 0 ? 29 : 28;
  var daysInMonth = [31, february, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  var yearDiff = endDate.getFullYear() - startYear;
  var monthDiff = endDate.getMonth() - startDate.getMonth();
  if (monthDiff < 0) {
      yearDiff--;
      monthDiff += 12;
  }
  var dayDiff = endDate.getDate() - startDate.getDate();
  if (dayDiff < 0) {
      if (monthDiff > 0) {
          monthDiff--;
      } else {
          yearDiff--;
          monthDiff = 11;
      }
      dayDiff += daysInMonth[startDate.getMonth()];
  }

  return yearDiff ;
}

//function yeras of experience
const getYearsOfExperience = async(workExperience) => {
    //get array of work
    var myRe = /^.* months.*$/igm;
    var myArray;
    var work = [];
    while ((myArray = myRe.exec(workExperience)) !== null) {
        work.push(myArray[0]);
    }

    if(work.length != 0){
     //get first and last index
        var first_index = work[0];
        var last_index = work[work.length-1];
        //work start
        var lastIndexRemove_after = last_index.split('to')[0];
        var workStartDate = lastIndexRemove_after.substring(lastIndexRemove_after.indexOf("-") + 1);
        //work end
        var firstIndexRemove_before = first_index.substring(first_index.indexOf("to") + 1);
        firstIndexRemove_before = firstIndexRemove_before.replace("o", "");
        var firstIndexRemove_after = firstIndexRemove_before.split(':')[0];
        
        //check condition
        if(firstIndexRemove_after.includes('Present')){
          var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];;
            var date = new Date();
          current_date = months[date.getMonth()]+'-'+ date.getFullYear();
        } else {
          current_date = firstIndexRemove_after;
        }

        //calculate work experience
        var date_start = new Date(workStartDate); 
        var date_end = new Date(current_date); 

        let work_start = date_start.getFullYear() + '-' +(date_start.getMonth() +1) + '-'
          + date_start.getDate();

        let work_end = date_end.getFullYear() + '-' +(date_end.getMonth() +1) + '-'
          + date_end.getDate() ;

        let total_years = dateDiff(work_start,work_end);
        var total_experience = total_years;
         
        // console.log(work);
        // console.log(first_index);
        // console.log(last_index);
        // console.log('Start work:',workStartDate);
        // console.log('End Work :',current_date);
        // console.log('Start work Date:',work_start);
        // console.log('End Work Date :',work_end)
        // console.log('Total years:',total_years)

    } else {
      //other resumes
      if(typeof workExperience === 'undefined' || typeof workExperience === 'undefined' ){
        var total_experience = 0;
        return  total_experience;
      } 
      let stringNumbers = workExperience.match(/\d+/g);
      
      var total_experience = '';
      //
      if(stringNumbers == null){
          total_experience = 0;
      } else {
        if(stringNumbers.length == 1){
          total_experience = stringNumbers[0];
        } else { 

          let appendTextYear = stringNumbers.map(i => i + ' years');
          let appendTextYear2 = stringNumbers.map(i => i + ' year');
          let appendTextYear3 = stringNumbers.map(i => i + ' Year');
          let appendTextYear4 = stringNumbers.map(i => i + ' Years');
          let convertArrayToNumber = stringNumbers.map(i=>Number(i));
          //check
          if(workExperience.includes(appendTextYear[0]) === true || workExperience.includes(appendTextYear2[0]) === true || workExperience.includes(appendTextYear3[0]) === true || workExperience.includes(appendTextYear4[0]) === true){
              total_experience = convertArrayToNumber[0];
          }  else {
            let sumOfExperience = convertArrayToNumber.reduce((x,item) => x + item,0);
            if(sumOfExperience.toString().length > 2 || sumOfExperience.toString().length == undefined || sumOfExperience.toString().length == 'undefined'){
              sumOfExperience = 0;
            }
            var total_experience = sumOfExperience;

          }
        }
      }
    }

    return total_experience;
      
}
var parser = {
  /*parseResume: function (file,savePath) {
    var objParseBoy = new ParseBoy(), savedFiles = 0;


    var onFileReady =  function (preppedFile) {
      objParseBoy.parseFile(preppedFile, function (Resume) {
        logger.trace('I got Resume for ' + preppedFile.name + ', now saving...');
        const resume = new CandidateResumes(Resume.parts);
                       resume.save();
         objParseBoy.storeResume(preppedFile, Resume, savePath, function (err) {
          if (err) {
            return logger.error('Resume ' + preppedFile.name + ' errored',err);
          }
          logger.trace('Resume ' + preppedFile.name + ' saved');

        })
      });
    }
    processing.run(file, onFileReady);
  }*/
  
  parseResume:async function (file,savePath,userId, frontendURL) {
    var resumeLink={}
    var objParseBoy = new ParseBoy(), savedFiles = 0;
    var onFileReady = async function (preppedFile) {
      objParseBoy.parseFile(preppedFile,async function (Resume) {
        logger.trace('I got Resume for testing resumeLink' + preppedFile.name + ', now saving...');
        resumeLink.resumePath=preppedFile.name;
        let resumeData=Object.assign(Resume.parts, resumeLink);
        //currently password is not taken by manual side so passing this static password on mail as well as on table.
        const password = '123456'
        const url = `${frontendURL}/login/candidate`
        const {phone, email, name, skills } = Resume.parts;

        /* Mail body and Subject start */
        resumeData.mailBody = `<div style = "margin-top: 20px;">
                                  <p style= "font-size : 1.8rem;">Welcome! <strong>${name},</strong></p> <br /> 
                                  <p>Your account is created on Virtual Employee portal, please use this link <a href=${url}>${ url } </a> to login with your email <strong>${email}</strong> and your password <strong>${password}<strong>.</p>
                                </div>`
        resumeData.mailSubject =  'Login on Virtual Employee Portal'
        /* Mail body and Subject end */

        const hashedPw = await bcrypt.hash(password, 12);
        resumeData.candidate_status=1
        resumeData.created_by= userId  /* adding loggedin userId in resumeData object */

      
        
         /****get work experience ****/
            let workExperience = resumeData.workExperience;
            var total_experience = await getYearsOfExperience(workExperience);
                
            if(total_experience.length === 0){
              total_experience = 0;
            } else {
              total_experience = total_experience;
            }
            
            //console.log(total_experience);
          /***ends check***/
        resumeData.total_experience=total_experience;
        resumeData.password = hashedPw /* adding default password for each candidate */
        
        let searchbyPhone;
        let duplicatePhones = 0;
        let searchbyEmail;
        let duplicateEmails = 0;
        
        if (phone) {
            const phoneno = new RegExp(RegExp.escape(phone.trim(), 'i'));
            searchbyPhone = { phone: phoneno }
            duplicatePhones = await CandidateResumes.find(searchbyPhone).countDocuments();
        }

        if (email) {
          const emailId = new RegExp(email.trim(), 'i');
          searchbyEmail = { email: emailId }
          duplicateEmails = await CandidateResumes.find(searchbyEmail).countDocuments();
        }

        // if(duplicatePhones >= 1 || duplicateEmails >= 1){
        //   console.log('duplicate');
        //   return false;
        // } 
        
        if(duplicatePhones === 0 && duplicateEmails === 0){
          const resume = new CandidateResumes(resumeData);
          resume.save();
          
          //send mail to each candidate content like link, email and password
          await sendMail(resumeData)

          const resumeStatus = new CandidateResumeStatus();
          resumeStatus.candidate_id       = resume._id;
          resumeStatus.candidate_status   = 1;
          resumeStatus.user_id            = userId;
          await resumeStatus.save();

          await checkSkillsData(skills);
        }        

        
        objParseBoy.storeResume(preppedFile, Resume, savePath, function (err) {
          if (err) {
            return logger.error('Resume ' + preppedFile.name + ' errored',err);
          }
          logger.trace('Resume ' + preppedFile.name + ' saved'); 
        })
      });
    }
    processing.run(file, onFileReady);
  },

  resumeCheck: async function (file,cbAfterParse){
    let output;
    var objParseBoy = new ParseBoy(), savedFiles = 0;
    var onFileReady = async function (preppedFile) {
      objParseBoy.parseFile(preppedFile, async function (Resume) {
        var resumeLink={};
        logger.trace('I got Resume for ' + 'url' + ', now sending...');
        resumeLink.resumePath=preppedFile.name;
         let resumeData=Object.assign(Resume.parts, resumeLink);
        //  resumeData.phone=1
        const {phone, email} = Resume.parts;
        let searchbyPhone;
        let duplicatePhones = 0;
        let searchbyEmail;
        let duplicateEmails = 0;

        if (phone) {
          const phoneno = new RegExp(RegExp.escape(phone.trim(), 'i'));
          searchbyPhone = { phone: phoneno }
          duplicatePhones = await CandidateResumes.find(searchbyPhone).countDocuments();
        }

        if (email) {
          const emailId = new RegExp(email.trim(), 'i');
          searchbyEmail = { email: emailId }
          duplicateEmails = await CandidateResumes.find(searchbyEmail).countDocuments();
        }

        if(duplicatePhones >= 1 || duplicateEmails >= 1){
          const {phone, email ,name} = Resume.parts;
          var fileData = {
            output : true,
            file: email
          }
          return cbAfterParse(fileData);
          //return cbAfterParse(email);
        } else {
          var fileData = {
            output : false,
            file: file
          }
          //let output = false;
          return cbAfterParse(fileData);
        }
  
      });

      // objParseBoy.parseFile(preppedFile, async function (Resume) {
      //   const {phone, email} = Resume.parts;
      //   let searchbyPhone;
      //   let duplicatePhones = 0;
      //   let searchbyEmail;
      //   let duplicateEmails = 0;
        
      //   if (phone) {
      //       const phoneno = new RegExp(phone.trim(), 'i');
      //       searchbyPhone = { phone: phoneno }
      //       duplicatePhones = await CandidateResumes.find(searchbyPhone).countDocuments();
      //   }

      //   if (email) {
      //     const emailId = new RegExp(email.trim(), 'i');
      //     searchbyEmail = { email: emailId }
      //     duplicateEmails = await CandidateResumes.find(searchbyEmail).countDocuments();
      //   }
      //    console.log("duplicatePhones",duplicatePhones);
      //   if(duplicatePhones === 0 && duplicateEmails === 0)
      //     output = true;
      //   else if(duplicatePhones != 0 || duplicateEmails != 0)
      //     output = false;
      // })
    }
    processing.run(file, onFileReady);
    return output;
  }

}
module.exports = parser;
